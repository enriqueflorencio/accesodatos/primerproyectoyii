<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\maillot */

$this->title = 'Create Maillot';
$this->params['breadcrumbs'][] = ['label' => 'Maillots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maillot-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
