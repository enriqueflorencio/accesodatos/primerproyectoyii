<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\puerto */

$this->title = 'Update Puerto: ' . $model->nompuerto;
$this->params['breadcrumbs'][] = ['label' => 'Puertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nompuerto, 'url' => ['view', 'id' => $model->nompuerto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="puerto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
